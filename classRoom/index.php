#!/usr/bin/php
<?php

require_once 'vendor/autoload.php';

use Symfony\Component\Console\Application;
use PrestaPHP\Command\CalcAverageCommand;

$application = new Application();
$application->add(new CalcAverageCommand());
$application->run();
