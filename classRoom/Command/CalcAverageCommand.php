<?php

namespace PrestaPHP\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CalcAverageCommand extends Command
{
    private $classRoom = [
        'Sarah' => [16, 18, 15, 17, 13],
        'Denis' => [4, 1, 8, 13],
        'Laurent' => [13, 20, 15, 11, 9],
    ];

    public function configure()
    {
        $this
            ->setName('presta:calc-average')
            ->addArgument('name', InputArgument::REQUIRED, 'name of studient')
        ;
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $name = $input->getArgument('name');
        if (!isset($this->classRoom[$name])) {
            throw new InvalidArgumentException('Unknown studient ' . $name);
        }

        $grades = $this->classRoom[$name];

        $maxGrades = array_reduce($this->classRoom, function ($studient, $carry = 0) {
            return count($studient) > $carry ? count($studient) : $carry;
        });

        $average = array_sum($grades) / count($maxGrades);

        if ($average >= 10) {
            $output->writeln("Passed : $name scored an average of $average");
            exit(0);
        }

        $output->writeln("Failed : $name scored an average of $average");
    }
}