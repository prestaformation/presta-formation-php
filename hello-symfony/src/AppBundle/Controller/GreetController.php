<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class GreetController extends Controller
{
    public function greetAction(Request $request, $name = 'world')
    {
        // $name = $request->attributes->get('name');
        return new Response("<html><body>Hello $name !</body></html>");
    }

    public function indexAction(Request $request)
    {
        // send mail
        // log in database

        return $this->redirectToRoute('greet');
    }
}
