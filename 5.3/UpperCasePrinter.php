<?php

namespace PrestaPHP;

require_once './Printer.php';

class UpperCasePrinter extends Printer
{
    protected static function format($message)
    {
        return strtoupper($message);
    }
}