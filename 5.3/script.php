<?php

require_once './UpperCasePrinter.php';

use PrestaPHP\UpperCasePrinter;

UpperCasePrinter::printLine('Hello world');
UpperCasePrinter::printLines(['Hello', 'World']);
