<?php

namespace PrestaPHP;

class Printer
{
    public static function printLine($message)
    {
        echo static::format($message) . "\n";
    }

    public static function printLines(array $messages)
    {
        array_walk($messages, function ($message) {
            static::printLine($message);
        });
    }

    protected static function format($message)
    {
        return $message;
    }
}