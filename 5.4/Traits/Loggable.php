<?php

namespace PrestaPHP\Traits;

trait Loggable
{
    public function log($message)
    {
        echo "Log: $message \n";
    }
}
