<?php

namespace PrestaPHP;

require_once './Traits/Loggable.php';

class Bar
{
    use Traits\Loggable;
}