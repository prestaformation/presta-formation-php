<?php

namespace PrestaPHP;

require_once './Traits/Loggable.php';

class Foo
{
    use Traits\Loggable;
}