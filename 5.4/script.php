<?php

require_once './Foo.php';
require_once './Bar.php';

(new PrestaPHP\Foo())->log('Hello');
(new PrestaPHP\Bar())->log('World');
